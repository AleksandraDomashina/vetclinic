import model.animal.*
import model.vet.Vet
import model.clinic.*

fun main() {
    val vetClinic = VetClinic(10)

    val fluffy = Cat("Fluffy","cat food", "House")
    val loyal = Dog("Loyal","dog food", "House", "bone")
    val candy = Horse("Candy","horse food", "Stable", _speed = 60)

    val vet = Vet("Ivan")

    val animals = arrayOf(fluffy, loyal, candy)   

    animals.forEach { animal ->
        val cabinet = vetClinic.takeAvailableCabinet()

		cabinet?.let {
			println("Cabinet #${it.number}")
			
			it.book(animal, vet)
			it.action()
			
			println("Cabinet #${it.number}, available: ${it.available}")
		}

		println()
    }
}