package model.animal

class Cat(
    name: String, 
    food: String,
    livingEnvironment: String, 
    val canScratch: Boolean = true
) : Animal(
	name, 
	food, 
	livingEnvironment
) {

    override fun makeNoise() {
        println("Meow")
    }    

    fun pat() {
        println("Patting ${name}")
    }
}