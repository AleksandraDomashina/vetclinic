package model.animal

abstract class Animal(
    val name: String, 
    val food: String, 
    val livingEnvironment: String
) {
    
    fun eat() {
        println("${name} has eaten ${food}")
    }

    fun sleep() {
        println("${name} is asleep")
    }

    open fun makeNoise() {
        println("Noises")
    }
}