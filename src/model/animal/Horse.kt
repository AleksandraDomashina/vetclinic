package model.animal

class Horse(
    name: String, 
    food: String,
    livingEnvironment: String, 
    val _speed: Int
) : Animal(name, food, livingEnvironment) {

    var speed: Int = 0
        set(value) {
            if (value>0) {
                field = value
            }
        }	

    init {
        speed = _speed
    }

    override fun makeNoise() {
        println("Neigh")

    }

    fun brush() {
        println("Brushing ${name}")
    }
}