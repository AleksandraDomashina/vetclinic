package model.animal

class Dog(
    name: String, 
    food: String, 
    livingEnvironment: String, 
    val toy: String
) : Animal(name, food, livingEnvironment) {
    
    override fun makeNoise() {
        println("Woof")
    }

    fun play() {
        println("${name} is playing with a ${toy}")
    }
}