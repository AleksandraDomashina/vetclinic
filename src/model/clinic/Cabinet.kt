package model.clinic

import model.animal.Animal
import model.vet.Vet

class Cabinet (val number: Int) {
    private var animal: Animal? = null
    private var vet: Vet? = null
	
    var available = true
		private set
		get() = (animal == null && vet == null)
		
    fun book(animal: Animal, vet: Vet) {
        this.animal = animal
        this.vet = vet
    }

    fun action() {
        animal?.let { animal ->
            vet?.let { it.treatAnimal(animal) } 
        }
    }
}