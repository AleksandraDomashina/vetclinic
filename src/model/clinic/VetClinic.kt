package model.clinic

class VetClinic(val cabinetAmount: Int) {
    private val cabinets = arrayListOf<Cabinet>()

    init {
        (1..cabinetAmount).forEach {
            cabinets.add(Cabinet(it))
        }
    }

    fun takeAvailableCabinet() = cabinets.firstOrNull {
        it.available
    }
}