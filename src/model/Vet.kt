package model.vet

import model.animal.*

class Vet(val name: String) {
    fun treatAnimal(animal: Animal) {
        println("${name} came to ${animal.name}") 
        
        animal.eat()

        when (animal) {
            is Cat -> {
                animal.pat()
                animal.makeNoise()
            }
            is Dog -> {
                animal.play()
                animal.makeNoise()
            }
            is Horse -> animal.brush()
        }
    }
}