# Vet Clinic

This app imitates work of vet clinic.

You need to have kotlin compiler, preferably version - 1.4.10, and JDK - 15

To compile everything at once:
```
kotlinc *.kt model\*.kt model\animal\*.kt model\clinic\*.kt -include-runtime -d VetClinic.jar
```
To run this project:
```
java -jar VetClinic.jar
```
